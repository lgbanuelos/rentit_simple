package demo.integration.dto;

import java.util.ArrayList;
import java.util.List;

import demo.models.Plant;

public class PlantResourceAssembler {
	public PlantResource toResource(Plant plant) {
		PlantResource res = new PlantResource();
		res.setId(plant.getId());
		res.setName(plant.getName());
		res.setDescription(plant.getDescription());
		res.setPrice(plant.getPrice());
		return res;
	}
	public List<PlantResource> toResource(List<Plant> plants) {
		List<PlantResource> ress = new ArrayList<>();
		
		for (Plant plant: plants)
			ress.add(toResource(plant));
		return ress;
	}
}

package demo.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name="plant")
public class PlantResource {
	Long id;
    String name;
    String description;
    Float price;
}

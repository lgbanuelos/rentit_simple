package demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import demo.models.Plant;

@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {

    List<Plant> findByNameLike(String name);

    @Query("select p from Plant p " +
           "where LOWER(p.name) like LOWER(:name) and p.price between :minimum and :maximum")
    List<Plant> finderMethod(@Param("name") String name, 
                             @Param("minimum") Float minimum,
                             @Param("maximum") Float maximum);
}